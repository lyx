# Textclass definition file for LaTeX.
# Author : Martin vermeer <martin.vermeer@hut.fi>
# Character Styles definition

Format 11

# Error fallback:
InsetLayout CharStyle
	LyxType               end
	LabelString           "UNDEFINED"
	Font
	  Color               error
	EndFont
End

