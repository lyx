// -*- C++ -*-
/**
 * \file SignalSlotPrivate.cpp
 * This file is part of LyX, the document processor.
 * Licence details can be found in the file COPYING.
 *
 * \author André Pönitz
 *
 * Full author contact details are available in file CREDITS.
 */

#include <config.h>

// trigger moc 
#include "SignalSlotPrivate.h"
#include "moc_SignalSlotPrivate.cpp"

